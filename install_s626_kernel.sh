#! /usr/bin/env bash

# Author: Adam Sperry

# This script is meant to make registering the s626 kernel driver
# with DKMS simple. Doing so means the driver does not need to be
# manually installed every time the linux kernel is updated.

# Helper functions
msg() {
    echo >&2 -e "${1-}"
}

die() {
    local msg=$1
    local code=${2-1} # default exit status 1
    msg "$msg"
    exit "$code"
}

# Make sure that DKMS has been installed
if ! command -v dkms &> /dev/null; then
    die "DKMS must be installed before running this script. (e.g. sudo apt install dkms)"
fi

# Make sure the script has been called as root
if [ "$(id -u)" != "0" ]; then
    die "This script needs to be run as root. (e.g. sudo ./install_s626_kernel.sh)"
fi

# Display summary text
echo
echo "This script will set up the s626 driver module with Dynamic Kernel Module";
echo "Support (DKMS) so there will be no need to install the driver manually";
echo "every time the linux kernel is updated.";
echo

# Get the absolute path to this script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Specify the source and install directories for the driver source files
srcDir=$SCRIPT_DIR/s626-1.0.5-modified
installDir=/usr/src/s626-1.0.5

# Copy the driver source files to the default DKMS directory
echo "Copying source files to $installDir...";
mkdir -p $installDir || die "ERROR Unable to create installation directory for driver source files."
cp $SCRIPT_DIR/dkms.conf $installDir || die "ERROR Unable to install driver source files to destination."
cp $srcDir/Makefile.module "$installDir/Makefile" || die "ERROR Unable to install driver source files to destination."
cp $srcDir/s626drv.c $installDir || die "ERROR Unable to install driver source files to destination."
cp $srcDir/s626drv.h $installDir || die "ERROR Unable to install driver source files to destination."
cp $srcDir/s626.h $installDir || die "ERROR Unable to install driver source files to destination."
cp $srcDir/s626types.h $installDir || die "ERROR Unable to install driver source files to destination."

# Add, build, and install the driver module
echo "Installing the driver module to DKMS tree...";
if ! dkms install -m s626 -v 1.0.5; then
    die "ERROR Failed to install the driver module."
fi

# Reload the driver module in case the Comedi driver module is already loaded
echo "Reloading the driver module...";
modprobe -r s626
modprobe s626

# Install the udev rules needed to create the device files at boot time
echo "Installing Udev rule...";
cp $SCRIPT_DIR/90-s626.rules /etc/udev/rules.d || die "ERROR Could not install Udev rule."

# Print final message
echo
echo "The s626 kernel driver module has been installed! You should verify that the";
echo "Comedi driver module is not loaded. You may also need to reboot your computer";
echo "to trigger Udev to create the device files in /dev.";
