**Contributors**:

 - Adam Sperry

---
## Sensoray 626 (s626) PCI Data Acquisition Card

The s626 is a data acquisition card that plugs into an open PCI slot on your
computer and enables digital and analog I/O. The card is most notably used to
control the SAMM but has been used elsewhere in our lab (e.g. to drive
Helmholtz coils). This card has been discontinued by Sensoray in favor of the
[s826 card](https://bitbucket.org/mmrobotics/s826-daq/src), a PCIe version
of the s626.

---
## Repository Contents

This repository contains all the documentation and source code necessary to set
up an s626 card on a Linux or Windows machine. The repository contains a modified
version of the Linux SDK to fix various problems with the original Linux SDK. The
contents of the repository are as follows:

 - `Documentation/` - Contains the documentation for the s626 card and its
                      breakout boards.
 - `s626-1.0.5-modified/` - The modified s626 Linux SDK source code. Contains
                            source code for both the kernel-level driver and the
                            user-level driver (referred to as the API Library).
 - `install_s626_kernel.sh` - A custom shell script used to set up and install the
                              kernel-level driver on Linux. This script replaces the
                              original kernel-level driver installation instructions
                              found in `s626-1.0.5-modified/README`.
 - `uninstall_s626_kernel.sh` - A custom shell script that reverses the installation
                                steps performed by `install_s626_kernel.sh`
 - `dkms.conf` - The DKMS configuration file used to set up the kernel-level driver
                 with DKMS on Linux (described below).
 - `90-s626.rules` - The Udev rules file used to ensure device files are created at
                     boot time on Linux (described below).
 - `CMakeLists.txt` and `cmake/` - The Linux user-level driver (or API library) has
                                   been made into a CMake project for convenience
                                   when building and installing the library and when
                                   linking to the library from your own projects.
 - `sdk_626_linux_1.0.5.tar.gz` - The original s626 Linux SDK containing the original
                                  source code. This archive file is present only to
                                  retain a copy of the original SDK; the modified
                                  SDK should be used by following the instructions
                                  below.
 - `sdk_626_win_1.0.5.zip` - The s626 Windows SDK. This repository is primarily directed
                             toward using the s626 on Linux. If you so desire, this
                             ZIP file contains the setup executable to install the
                             drivers on Windows (presumably both the kernel-level
                             and user-level drivers).

---
## Documentation
The `Documentation/` directory contains copies of all the documentation offered
by Sensoray for the s626 on its
[product page](http://www.sensoray.com/products/626.htm) and
[technical support wiki](http://www.sensoray.com/wiki/index.php?title=626). Make
sure to read `Documentation/Instruction_Manual.pdf` and
`Documentation/API_Reference.pdf` at a minimum. Note that while
`Documentation/API_Reference.pdf` says it is for Windows, it is relevant for
Linux because the same API is used in both operating systems.

Since the Sensoray 626 is a **legacy** product, don't expect the above links to
be valid forever. You may need to do a Google search. Despite this, efforts were
made to put copies of all the documentation into this repository and it is not
expected that new information will be posted online due to the card's legacy
status.

---
## Installation and Usage on Linux

The s626 driver consists of two parts: the kernel-level driver and the user-level
driver (referred to as the API library). The process of using an s626 card on
Linux involves installing the kernel-level driver, compiling the API library,
then linking to the API library in your own source code so you can interact with
the card using the s626 API functions (see `Documentation/API_Reference.pdf` for
API function documentation). This repository contains some modifications from
the original SDK to fix some issues and make setup/usage easy. Please
follow the instructions below to set up and use the s626 on Linux. For reference,
`s626-1.0.5-modified/README` contains the original driver setup instructions from
Sensoray, but again, the instructions below are intended to replace the instructions
in `s626-1.0.5-modified/README`. 

### Installing the Kernel-Level Driver

> :warning: **The modified version of the kernel-level driver has been verified to work on Ubuntu versions 16.04, 18.04, 20.04, and 22.04. The driver may work on later versions of Ubuntu, but there is no guarantee. Patches to the driver may be necessary to support later Ubuntu versions.**

#### A Note on Secure Boot

If the following instructions do not work, you may need to disable Secure Boot from
the EFI BIOS on your computer. Just hold the delete key or whatever key is necessary
to enter your EFI BIOS settings when the computer first boots up. With Secure Boot
on, you won't be able to load kernel modules (drivers) from 3rd party vendors that
are not signed with a special key. This s626 driver is not normally signed with the
special key, however, DKMS (explained below) should sign the s626 driver with a key
so it should not be necessary to disable Secure Boot if you follow the instructions
below. Kernel module key signing has recently started being enforced in Ubuntu and
is something to be aware of with modern Ubuntu.

#### Installation Instructions

Make sure that you have installed `build-essential` and `dkms` with
the following command:

    sudo apt install build-essential dkms

The `install_s626_kernel.sh` script is intended to make the kernel-level driver
installation simple. It will set up the driver source code for compilation and
installation using DKMS. Simply run the script with root privileges and read the
output carefully to verify that the installation was successful.

    cd s626-daq
    sudo ./install_s626_kernel.sh

As a reminder, this install script is intended to replace the original installation
methods described in `s626-1.0.5-modified/README`.

#### Verifying the Installation

The Ubuntu distribution comes with a preinstalled s626 driver which **does not
work** if your source code uses the API Library from this repository to interact
with the card. The preinstalled driver is part of the Comedi collection of drivers
and you would need to use their API to use their driver. However, much of the code
in our lab uses the API Library from this repository so such code will not function
with the default-installed Comedi driver.

There are several terminal commands that can be used to display info about kernel
level drivers:

 - `lsmod` - lists all the drivers that are currently loaded. The name of the s626
             driver is `s626`.
 - `modinfo <driver name>` - used to get information about a particular driver.

After the installation script has finished, use `modinfo s626` to verify that the
correct driver is installed. If the author field in the output of `modinfo s626` says
"David Stroupe and Charlie X. Liu", then the driver has been installed successfully.

To verify that the driver is loaded (i.e. your computer has identified that an s626
card is present) verify that "s626" is in the output of `lsmod`. This can easily be
done using `lsmod | grep s626` and checking that the output says "s626". If there is
no output, you may need to reboot the computer.

#### Description of Kernel-Level Driver Fixes

The problems with the original kernel-level driver and its installation process are
as follows:

 - The driver does not compile without some changes to the source code.
 - The device files created in `/dev/`(e.g., `/dev/s626a`, `/dev/s626a0`,
   `/dev/s626a1`), which are used to talk to the s626 card, are created during the
   driver installation process and disappear on reboot. This means you would need to
   manually go through the installation process again every time you restart your
   computer.
 - The kernel-level driver installation does not carry over when the Linux kernel
   is updated (which happens frequently when the OS is updated). This means the
   driver may need to be manually installed again if the OS is updated.

In order for the driver to compile, the following line needed to be replaced in
the `s626drv.c` file:

    #include <linux/version.h>
    
was replaced with

    #include <linux/version.h>
    #include <linux/uaccess.h>

Further, some of the functions used were deprecated and removed in the Linux kernel
version 5.18. A large block of code was added to make the driver support Linux kernel
versions with these functions removed.

To fix the device file problem, the `90-s626.rules` udev rule is installed by the
`install_s626_kernel.sh` script which tells udev to run some bash commands when the
system detects the presence of an s626 card at boot time. These bash commands create
the device files and were copied exactly from the `s626-1.0.5-modified/MAKEDEV`
script that Sensoray uses to generate the device files when following their original
installation process. The udev rule gets installed in `/etc/udev/rules.d`.

To fix the issue with OS updates, the `install_s626_kernel.sh` script sets up the
driver to be built and installed using DKMS. DKMS (Dynamic Kernel Module Support)
is very useful as it will register the driver to be reinstalled automatically every
time the kernel is updated. A good resource to learn more about DKMS and how it
works is to read the manual by typing `man dkms` into the terminal.

For further understanding, study up on the following topics: `udev`, `sysfs`,
`/dev` directory, `devtempfs`, `devfs`, `systemd`, module commands (`lsmod`,
`modinfo`, `insmod`, `rmmod`, `modprobe`), and general Linux driver development.

### Compiling and Using the API Library (User-Level Driver)

Once the kernel-level driver has been installed, the API library (User-Level
Driver) needs to be compiled and linked to your project. For convenience, a CMake
project has been created to build and install the library, making it easy to use in
your own project. Alternatively, you can directly use Sensoray's Makefile
`s626-1.0.5-modified/Makefile` to build the API library (see instructions below).
However, it is recommend that you build and install the library using CMake.

> **Note:** The [Robotics Framework](https://bitbucket.org/mmrobotics/roboticsframework/src/master/) expects that the API library will be built and installed using CMake.

#### Building and Installing with CMake

To build and install the API library with CMake, simply execute the following
commands from the top level of this repository (e.g. `cd s626-daq`):

```
mkdir build
cd build
cmake ../
sudo make install
```

These commands create a `build` directory at the top level of the repository
and execute the build from inside that directory. They then install the library
into `/usr/local/` (the default installation path for CMake). This location makes
the library available to any user on the computer, but requires `sudo` privileges.
If desired, the installation path can be changed by setting the
`CMAKE_INSTALL_PREFIX` CMake variable as follows:

```
mkdir build
cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=/home/john/packages/s626
make install
```

The above commands will install into `/home/john/packages/s626` which does not
require `sudo` privileges. However, be aware that if you chose to install in a custom
location, you will need to understand how to use the `CMAKE_PREFIX_PATH` CMake
variable to let CMake know where the API library is located when building projects
that use the library. Therefore, it is recommended to stick with the default location
of `/usr/local/`.

To link against the API library, use `find_package()` and `target_link_libraries()`
in your `CMakeLists.txt` file:

```
find_package(s626 REQUIRED)

# Create a CMake target called "myTarget"

target_link_libraries(myTarget s626::s626)
```

#### Building with Sensoray's Makefile

To compile simply call `make lib` from inside the SDK source directory
`s626-1.0.5-modified/`

    cd s626-1.0.5-modified
    make lib

This will create the static library `libs626.a` in the source directory.

Linking against the library is done in your preferred way. For linking with
compiler flags:

 - Make sure the directory where the library resides is included in the
   search using the `-L` compiler flag
 - Use the `-ls626` compiler flag when linking


#### Including the API in source code

Include the API in source code using:

    extern "C" {
	    #include "App626.h"
	    #include "s626api.h"
    }

This will make the API functions available to use. See
`Documentation/API_Reference.pdf` for API function documentation.

Note that if you built using Sensoray's Makefile, you will need to make sure the SDK
source directory is in your list of include directories (e.g. using the `-I`
compiler flag).
